package AppCycles.flows;

abstract class baseJob implements Serializable {

    def script
    def currentStage

    baseJob(script) {
        this.script = script
    }
    
    void run() {
        script.timestamps() {
            try {
                script.echo "Running with params: ${this.properties}"
                runImpl()
            } finally {
                try {
                } catch (all) {
                }

            }
        }
    }

    void runImpl() {
    }

    void runStage(String name, Closure stage) {
        script.log.anchor("${name} stage start")
        currentStage = name
        script.stage(name, stage)
        script.log.anchor("${name} stage end")
    }
}
