import AppCycles.flows.appCyclesComponentFlow

def call(Map config) {
    if (config == null) {
        config = [:]
    }
    if (config.sleep == null)
    {
        config.sleep = 0
    }
    if (config.quietPeriod == null)
    {
        config.quietPeriod = 5
    }
    if (config.numToKeepStr == null)
    {
        config.numToKeepStr = "5"
    }

    pipeline {
        agent { label 'jenkins-jnlp-slave' }
        options {
            timestamps()
            buildDiscarder(logRotator(numToKeepStr: config.numToKeepStr))
            ansiColor('xterm')
            skipDefaultCheckout()
            disableConcurrentBuilds()
            quietPeriod(config.quietPeriod)
        }
        stages {
            stage ("[Pipeline setup]") {
                steps {
                    script {
                        checkout scm
                    }
                }
            }
            stage ("[Component Flow]") {
                steps {
                    script {
                        def ciJob = new AppCycles.flows.appCyclesComponentFlow(this)
                        ciJob.run()
                    }
                }
            }
        }
        post {
            always {
                script {
                    sleep (config.sleep)
                }
            }
        }
    }
}